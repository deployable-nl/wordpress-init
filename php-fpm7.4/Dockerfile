FROM php:7.4-fpm-alpine3.16

SHELL ["/bin/ash", "-xeo", "pipefail", "-c"]

WORKDIR /opt/deployable

RUN addgroup -Sg 101 nginx; \
    adduser -H -h /dev/null -u 101 -G nginx -S -s /sbin/nologin -g nginx nginx; \
    adduser www-data nginx; \
    apk add --no-cache \
        bash \
        freetype \
        gettext \
        jpeg \
        libmcrypt \
        libpng \
        libzip \
        tzdata; \
    apk --update add --no-cache --virtual .build-deps \
        autoconf \
        build-base \
        freetype-dev \
        gettext-dev \
        jpeg-dev \
        libmcrypt-dev \
        libpng-dev \
        libzip-dev; \
    docker-php-ext-configure gd --with-freetype --with-jpeg; \
    docker-php-ext-install opcache gd mysqli sockets gettext; \
    pecl install mcrypt redis; \
    docker-php-ext-enable mcrypt redis; \
    apk del .build-deps; \
    docker-php-source delete; \
    rm -r /usr/local/bin/docker-php-source /usr/local/bin/docker-php-ext-* /usr/local/bin/php-config /usr/local/bin/pecl /usr/local/bin/pear /usr/local/bin/peardev /usr/local/bin/phpize /usr/local/lib/php/*.php /usr/local/lib/php/Archive /usr/local/lib/php/Console /usr/local/lib/php/OS /usr/local/lib/php/PEAR /usr/local/lib/php/Structures /usr/local/lib/php/XML /usr/local/lib/php/build /usr/local/lib/php/data /usr/local/lib/php/doc /usr/local/lib/php/test /tmp/pear;

COPY --chown=root:www-data php.ini /usr/local/etc/php/conf.d/deployable.ini
COPY --chown=root:www-data php-fpm.conf /usr/local/etc/php-fpm.d/zzz-deployable.conf

VOLUME ["/tmp", "/var/run/php"]
