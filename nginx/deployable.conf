server_tokens off;
set_real_ip_from 10.0.0.0/8;
real_ip_header proxy_protocol;

upstream php-fpm {
    server unix:/run/php/php-fpm.sock;
}

map $http_x_forwarded_proto $thescheme {
    default $scheme;
    https https;
}

map $http_x_forwarded_proto $ishttps {
    default off;
    https on;
}

server {
    listen 8000 proxy_protocol;
    listen 8080;

    server_name _;
    
    root /opt/deployable/wordpress/public_html;
    index index.php;

    client_max_body_size 200m;

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    
    location ~ \.php$ {
        fastcgi_index index.php;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param  REQUEST_SCHEME     $thescheme;
        fastcgi_param  HTTPS              $ishttps;
        fastcgi_pass php-fpm;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires max;
    }
}
